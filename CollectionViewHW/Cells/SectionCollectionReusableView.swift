//  SectionCollectionReusableView.swift
//  CollectionViewHW
//  Created by Viktoriia Skvarko on 10.02.2021.


import Foundation
import UIKit

class SectionCollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var sectionNameLable: UILabel!
    
}
