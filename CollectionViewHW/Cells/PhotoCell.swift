//  PhotoCell.swift
//  CollectionViewHW
//  Created by Viktoriia Skvarko on 10.02.2021.


import UIKit

class PhotoCell: UICollectionViewCell {
    
    @IBOutlet weak var imageCell: UIImageView!
    
}
