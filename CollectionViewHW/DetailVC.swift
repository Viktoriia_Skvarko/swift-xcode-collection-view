//  DetailVC.swift
//  CollectionViewHW
//  Created by Viktoriia Skvarko on 10.02.2021.


import UIKit

class DetailVC: UIViewController {
    
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var informDetail: Photo?
    
    @IBOutlet weak var imageDetailLable: UIImageView!
    @IBOutlet weak var nameDetailLable: UILabel!
    @IBOutlet weak var categoryLable: UILabel!
    @IBOutlet weak var dataLable: UILabel!
    @IBOutlet weak var cameraLable: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageDetailLable.image = UIImage(named: informDetail!.imageName)
        nameDetailLable.text = informDetail?.name
        categoryLable.text = informDetail?.category
        dataLable.text = informDetail?.data
        cameraLable.text = informDetail?.camera
    }
}
